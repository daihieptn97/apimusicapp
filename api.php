<?php
require_once('HandlerData.php');
doAction();

function doAction()
{
    $typeMusic = isset($_GET['type']) ? $_GET['type'] : false;


    $handlerData = new HandlerData();

    if ($typeMusic) {
        $handlerData->getListFileMusic($typeMusic);
    } else {
        $handlerData->getAllMusic();
    }
}

