<?php



class HandlerData
{

    public function getListFileMusic($dir)
    {

        $this->doGetFile("./" . $dir . "/");
    }

    public function getAllMusic()
    {
        $dir = './';
        /*echo "<pre>";
        var_dump();
        echo "</pre>";*/
        $dataFile = scandir($dir);
        $res = array();
        foreach ($dataFile as $fileItem) {
            if (is_dir($dir . $fileItem . "/") && strlen($fileItem) >= 2 && !is_numeric(strpos($fileItem, ".", 0))) {
                $res += $this->loadFile($dir . $fileItem . "/", $res);
            }
        }
        echo $this->jsonStructure($res);
    }

    private function getTypeFile($nameFile = '.')
    {
        $d = explode(".", $nameFile);

        if (count($d) > 1 && strlen($d[0]) > 0) {
            return $d[count($d) - 1];
        }
        return false;
    }

    private function doGetFile($dir = "")
    {
        $res = array();
        if (!file_exists($dir)) {
            echo $this->jsonStructure($res, "Thư mục không tồn tại.");
            die();
        }
        $res = $this->loadFile($dir, $res);
        echo $this->jsonStructure($res);
    }

    private function loadFile($dir, $res)
    {
        $dataFile = scandir($dir);
        $nameFolder = substr($dir,
            strpos($dir, "/") + 1,
            strpos($dir, "/", strpos($dir, "/") + 1) - 2
        );
        $actual_link = "http://$_SERVER[HTTP_HOST]/musich/$nameFolder/";
        foreach ($dataFile as $nameFile) {
            if ($this->getTypeFile($nameFile)) {
                $t = date("d/m/Y - H:i:s", @filemtime($dir . $nameFile));
                $r = (object)[
                    "name" => $nameFile,
                    "type" => $this->getTypeFile($nameFile),
                    "date" => $t,
                    "link" => $actual_link . str_replace(" ", "%20", $nameFile)
                ];

                array_push($res, $r);
            }
        }

        return $res;

    }

    private function jsonStructure($data = [], $message = "Không có bài hát nào")
    {
        if (count($data) > 0) {
            return json_encode((object)["status" => true, "message" => "Lấy dữ liệu thành công.", "data" => $data]);
        }
        return json_encode((object)["status" => false, "message" => $message, "data" => []]);
    }
}